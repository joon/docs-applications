============
 foobar2000
============

Componenets
===========

* `Columns UI <http://yuo.be/columns.php>`_

  * You can install this to your ``%appdata%\foobar2000\user-components\foo_ui_columns``

* `Quick Tagger <http://www.foobar2000.org/components/view/foo_quicktag>`_
* `Windows 7 Shell Integration <http://wintense.com/plugins/foo_w7shell>`_

External Programs
=================

* `flac <https://xiph.org/flac/download.html>`_

Useful Scripts
==============

http://nujufoobar.blogspot.com/2013/01/useful-scripts-foobar2000.html

Rating column
-------------

.. code-block:: text

   $rgb(0,128,255,0,128,255)$repeat(¡Ú,$meta(rating)) 
   $rgb(192,192,192,128,128,128)$repeat(¡Ù,$sub(5,%rating%))



