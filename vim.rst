=====
 Vim
=====

.. highlight:: vim

File marks
==========

GNU/Linux
---------

``~/.viminfo``::

   # File marks:
   'B  25  0  ~/.bashrc
   'E  366  0  ~/.emacs
   'G  42  15  ~/.gvimrc
   'U  5  8  ~/.unison/settings.prf
   'V  199  8  ~/.vimrc
   'Z  188  11  ~/.zshrc


Windows
-------

``~\_viminfo``::

   # File marks:
   'A  31  0  ~\Documents\AutoHotkey\AutoHotkey.ahk
   'B  36  0  ~\.bashrc
   'E  50  0  ~\.emacs
   'G  62  0  ~\.gvimrc
   'P  77  0  ~\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1
   'U  1  0  ~\.unison\Settings-win.prf
   'V  146  0  ~\.vimrc

