======
 Kile
======

Live Preview
============

The Live Preview feature only works with developmental version of Kile.
in openSUSE, you have to install the one at ``KDE:Unstable:Playground`` repo.

Also, if you have previous version of Kile installed, it might miss the
settings for Live Preview. Go to ``Settings`` > ``Configure Kile ..`` >
``Tools`` > ``Build`` and select ``Restore Default Tools..`` to refresh
settings.


