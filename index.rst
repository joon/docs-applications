.. Applications documentation master file, created by
   sphinx-quickstart on Sun Apr 21 11:38:21 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Applications's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

   bitbucket
   cli
   emacs
   firefox
   foobar2000
   git
   kile
   lyx
   qualtrics
   readthedocs
   sublime-text
   sync
   vim


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

