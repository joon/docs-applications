=======
 Emacs
=======

.. highlight:: common-lisp

org-mode
========

.. code-block:: common-lisp

   #+TITLE:     Project: Name
   #+AUTHOR:    Joon Ro
   #+EMAIL:     joonhyoung.ro@gmail.com
   #+DESCRIPTION: org file for the project
   #+CATEGORY: CATEGORY
   #+STARTUP: overview
    
   * Literature
   * Feedback
   * Tasks
   * Logs
   * Schedules
   * Notes
   * Events



CUA-Mode
========

.. code-block:: common-lisp

   (cua-mode t)
   (setq cua-auto-tabify-rectangles nil)  ; Don't tabify after rectangle commands
   (setq cua-keep-region-after-copy t)  ; Standard Windows behavior


Clipboard
=========

.. code-block:: common-lisp

   (transient-mark-mode 1)  ; no region when it is not highlighted
   (setq select-active-regions nil)  ; stops active region automatically setting primary selection

   ; OS dependent settings
   (cond ((eq system-type 'gnu/linux) 
            (progn
              (setq interprogram-paste-function 'x-cut-buffer-or-selection-value)
            ))
         ((eq system-type 'windows-nt)
            (progn
              (setq interprogram-paste-function 'x-selection-value)
            ))
   )


Auto Complete
=============

Tab completion does not work in org-mode
----------------------------------------

http://lists.gnu.org/archive/html/emacs-orgmode/2011-02/msg00180.html

org mode somehow (I have no idea how or why) manages to distinguish between
TAB and C-i and this distinction is inherited by minor modes operating within
an org mode buffer.  For auto-complete, the fix is easy::

   (require 'auto-complete)
   (require 'auto-complete-config)
   (ac-config-default)
   ;; (define-key ac-complete-mode-map "\t" 'ac-expand)
   (define-key ac-complete-mode-map [tab] 'ac-expand)     ; <<--- add this line
   ;; (define-key ac-complete-mode-map "\r" 'ac-complete)

I've included two commented out lines: they are the defaults set by
auto-complete.  I found, before adding the annotated line, that C-i worked as
expected within an auto-complete pop-up but the tab key didn't.  With this key
binding, it does.

Windows specific
================

Aspell
------

http://www.emacswiki.org/emacs/AspellWindows

#. Download Aspell executable (``Full installer``) and at least one dictionary
   (``aspell-en-0.50-2-3.exe``) from http://aspell.net/win32/.
#. Install ``Aspell-0-50-3-3-Setup`` and then ``Aspell-en-0.50-2-3``.
#. Add the following to your ``~/.emacs`` file::

   (add-to-list 'exec-path "C:/Program Files (x86)/Aspell/bin/")
   (setq ispell-program-name "aspell")
   (setq ispell-personal-dictionary "~/.emacs/.ispell")
   (require 'ispell)

org-mode: Google calendar sync
------------------------------

#. Install ``awk``
#. Install ``cron`` for windows
#. Install ``cURL``::

   $ cinst curl



