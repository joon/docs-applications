=====
 LyX
=====

.. highlight:: sh

View/Updating PDF
=================

- When you have sections in separate files, View/Update PDF will only compile
  the section file separately, and View/Update Master Document will compile
  everything. The references will only work when you compile the master.


Beamer
======

Lyx color schemes for the beamer title/frametitle/etc

http://www.mail-archive.com/lyx-users@lists.lyx.org/msg88941.html

You have to change the ``beamer.layout`` file. In the ``Style BeginFrame`` layout 
definition, change::

   Font
       Series         Bold
       Size           Largest
       Color          Blue
   EndFont

to whatever you need.

For single documents, you can also paste something like::

   Style Title
     Font
       Color          Green
     EndFont
   End
    
   Style Author
     Font
       Color          Green
     EndFont
   End
    
   Style Institute
     Font
       Color          Green
     EndFont
   End
    
   Style BeginFrame
     Font
       Color          Green
     EndFont
   End

to ``Document`` > ``Seetings`` > ``Local Layout``, hit ``Validate`` and ``Apply``.

Impressive
==========

``pdftk`` Version ``1.44-16.1.1`` worked, but 1.45 version did not work.

openSUSE
--------

* If you have ``pdftk`` 1.45 version, downgrade it.
* Use numpy version from devel:python repo. Otherwise it raises an error.

Windows
-------

* You can download older version of ``pdftk`` from
  http://www.pdflabs.com/docs/pdftk-version-history/. Make sure you get 1.44.



Windows
=======

Inverse Search
--------------

http://wiki.lyx.org/LyX/SyncTeX#toc3

#. Set ``Tools`` > ``Preferences`` > ``Paths`` > ``LyxServer pipe`` to
   ``\\.\pipe\lyxpipe``.
#. In ``Document`` > ``Settings`` > ``Output``, check ``Synchronize with output``.
#. Create a batch-file named ``lyxeditor.cmd`` and save it to one of the
   locations in your ``PATH`` Windows environmental variable:
  
   .. code-block:: bat
    
      @echo off
      SETLOCAL enabledelayedexpansion
      set file=%1
      set row=%2
      REM remove quotes from variables 
      set file=!file:"=!
      set row=!row:"=!
      %comspec% /q /c (@echo LYXCMD:revdvi:server-goto-file-row:%file% %row%)> \\.\pipe\lyxpipe.in&&type \\.\pipe\lyxpipe.out
      endlocal   

AutoHotkey Script
~~~~~~~~~~~~~~~~~

The above cmd works well, but it shows annoying black cmd window everytime
when you invoke the script. With a simple AutoHotkey script, one can not only
suppress this window, but also activate LyX windows after the inverse search.

Create a AutoHotkey script named ``lyx-inverse-search.ahk`` with the following
code and save it to the same location to ``lyxeditor.cmd`` and compile it with
AutoHotkey to generate ``lyx-inverse-search.exe``:

.. code-block:: ahk

   SetTitleMatchMode, RegEx
   Run, lyxeditor.cmd "%1%" "%2%",, Hide
   WinActivate, ^LyX:,,,

If you don't have AutoHotkey installed, you can download the compiled `exe
<https://dl.dropboxusercontent.com/u/561594/lyx-inverse-search.zip>`_.

SumatraPDF
~~~~~~~~~~

http://wiki.lyx.org/Windows/LyXWinTips#toc6

#. Download and install `Sumatra PDF
   <http://blog.kowalczyk.info/software/sumatrapdf/download-free-pdf-viewer.html>`_.
#. In LyX go toTools > Preferences > Paths and add the install location to ``PATH``
   prefix. Most likely this is ``C:\Program Files\SumatraPDF``. In my case, it
   is ``C:\Users\Programs\SumatraPDF``.

#. In ``Tools`` > ``Preferences`` > ``File Handling`` > ``File Formats``
   select ``PDF (pdflatex)`` from the list of formats and modify ``Viewer`` to
   the following (include everything after the colon, including quotes:

   .. code-block:: bat

      SumatraPDF -inverse-search "lyxeditor.cmd \"%f\" \"%l\""

#. If you have the compiled AutoHotkey script from above, use the following:

   .. code-block:: bat

      SumatraPDF -inverse-search "lyx-inverse-search.exe \"%f\" \"%l\""

Okular
~~~~~~

#. In LyX go to ``Tools`` > ``Preferences`` > ``Paths`` and add the install
   location to ``PATH`` prefix. In my case, it is ``D:\Users\Joon\Programs\KDE\bin``.
#. In Okular, ``Settings`` > ``Configure Okular`` > ``Editor``, choose
   ``Custom Text Editor`` and input the following:

   .. code-block:: bat

       lyxeditor.cmd "%f" "%l"

   If you have the compiled AutoHotkey script from above, use the following:
  
   .. code-block:: bat

       lyx-inverse-search.exe "%f" "%l"

.. note:: If ``lyxeditor.cmd`` or ``lyx-inverse-search.exe`` is not in your
          ``PATH``, then you have to specify the full path here.


GNU/Linux
=========

Screen Fonts
============

For the document in the LyX editor windows to look like an article, use::

   Latin Modern Roman
   Latin Modern Sans
   Latin Modern Typewriter

Okular and Inverse Search
-------------------------

http://wiki.lyx.org/LyX/SyncTeX#toc3
