=====
 Git
=====

.. highlight:: sh

Local
=====

Cancel Last Commit
------------------

http://superuser.com/questions/35267/how-can-i-roll-back-1-commit

If you have committed junk but not pushed::

   git reset --hard HEAD~1

Note that when using ``--hard`` any changes to tracked files in the working tree
since the commit before head are lost.

If you don't want to wipe out the work you have done, you can use ``--soft``
option that will delete the commit but it will leave all your changed files
"Changes to be committed", as git status would put it::

   git reset --soft "HEAD^"


Dealing with line endings
-------------------------

https://help.github.com/articles/dealing-with-line-endings

if ``git diff`` shows the whole file, you should set the following option::

   $ git config --global core.autocrlf input


Ignore mode changes (chmod)?
----------------------------

`How do I make git ignore mode changes (chmod)?
<http://stackoverflow.com/questions/1580596/how-do-i-make-git-ignore-mode-changes-chmod>`_::

   $ git config core.fileMode false


Export snapshot of previous revision
------------------------------------

.. code-block:: sh

   git archive rev > file   

      --prefix=dir/        Nest all files in the snapshot in directory 
      --format=[tar|zip]   Specify archive format to use: tar or zip

for example::

   $ git archive 778b252 -o snapshot.tar --prefix=snapshot/

another example (with automatic extraction)::

   $ git archive 778b252 -o snapshot.tar --prefix=snapshot/ && ex snapshot.tar && rm snapshot.tar && cd snapshot

the same example in PowerShell::

   $ git archive 778b252 -o snapshot.tar --prefix=snapshot\; Expand-Archive .\snapshot.tar; rm snapshot.tar; cd snapshot

GUI frontends
-------------

SmartGit
~~~~~~~~

Good inline diff for LaTeX source.

SourceTree
~~~~~~~~~~

Pleasant GUI


git-cola
~~~~~~~~

Very easy to select lines and stage.

There is Windows installer at:
https://github.com/git-cola/git-cola/downloads


Remote
======

Delete remote branches
----------------------

.. code-block:: sh

   git push origin :branchname

Commit Message
==============

.. code-block:: sh

   ! changed
   - removed
   + added
   * fixed

Workflow: gitflow
=================

http://nvie.com/posts/a-successful-git-branching-model/

- Have ``master`` and ``devel``
- Work on a feature branch for each task, branching out from ``develop``. Also
  make the branch name self-exploratory
- Work on the branch, make commits
- When the task is completed, merge the branch into ``devel``, with the
  following options:

    - ``Include messages from commits being merged in merge commit``

      This will list the commit message titles in the merge commit message

    - ``Create a new commit even if fast-forward is possible``

      This will make another commit explicitly saying Merge branch "whatever",
      so it is visually explicit. In cli, this is equivalent to::

         $ git merge --no-ff

- Then your ``devel`` branch's commit history would nicely reflect your
  workflow - and also it will have commits only when there is significant
  changes so it is much easier to look at.
- When it is ready to release, make another branch with ``release``.

- You have to merge in changes from ``devel`` often to get the newest changes
  from other branches.
- You can rebase instead of merge 


Main branches
-------------

master
~~~~~~

develop
~~~~~~~

Supporting branches
-------------------

feature
~~~~~~~

release
~~~~~~~

hotfix
~~~~~~



Workflow: Paper
===============

- Since a paper has many sections, I think the feature branches should be
  divided into section names. For example, instead of using ``feature/`` for
  everything, use actual section names such as ``estimation/`` and ``results/``.
- Or, it might be better to have ``edition``, ``writing`` since sometimes an
  edition is across many sections.

