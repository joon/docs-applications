===========
 Qualtrics
===========

Change ``.css`` settings per page
=================================

http://www.qualtrics.com/university/researchsuite/coders-corner/html-and-css

Common Qualtrics CSS Classes

The following are commonly used Qualtrics classes and IDs which can be
modified through CSS. Additional classes and IDs can be found using the
Inspect Element option in your browser, or through a third party extension
such as Firebug.

* ``#SurveyEngineBody``: The background of the entire survey page.
* ``.Skin .SkinInner``: The container for the questions area as well as the header
  and footer.
* ``.Skin #SkinContent``: The background for the questions area of the survey.
* ``.Skin #LogoBar``: The container for the logo on your survey.
* ``html .Skin #Logo``: The logo for your survey.
* ``.Skin #Buttons``: The next and previous buttons.
* ``.Skin .QuestionText``: The question text.

.. note:: In `Example Style Sheet
   <http://www.qualtrics.com/university/researchsuite/example-style-sheet>`_
   page, *Can Customize Width, Font* example is wrong (``.SkinInner`` should
   be ``.Skin .SkinInner``) - 04/23/2013
