===================
 Microsoft Windows
===================

Excel does not open to full screen when opened
==============================================

http://answers.microsoft.com/en-us/office/forum/office_2010-office_install/excel-does-not-open-to-full-screen-when-opened/a7405b25-1f2f-4465-ba64-f80a8adaae64

Add this a macro in ``Personal.xlsb``:

.. code-block:: vba

   ThisWorkbook:

   Private Sub Workbook_Open()
    
       Application.WindowState = xlMaximized
    
   End Sub

