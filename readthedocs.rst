===============
 Read the Docs
===============

BitBucket Settings
==================

BitBucket
---------

* Make sure you uncheck ``private`` from the repo
* Repo settings -> ``Hooks`` -> Add ``Read the Docs`` hook

Read the Docs
-------------

#. Import
#. For ``Repo:``, use::

      https://bitbucket.org/username/project

