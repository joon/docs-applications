===========
 BitBucket
===========

Wiki
====

Clone the wiki
--------------

Use the following URL for wiki::

   git@bitbucket.org:username/repo.git/wiki


Adding Images
-------------

Use absolute path::

   .. image:: ./images/Dead-Rising-2.png

.. note:: It seems it does not work if you have spaces in the image name.
