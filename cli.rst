========================
 Command-line interface
========================

.. highlight:: sh


find
====

Find files with filename and exec command against them
------------------------------------------------------

Delete found files::

   $ find ./ -maxdepth 1 -name "8*" -delete
   $ find ./ -maxdepth 1 -name "8*" -exec rm {} \;

Move found files::

   $ find ./ -maxdepth 1 -name "8*" -exec mv {} tmp/ \;

perl
====

Perl Pie
--------

.. code-block:: sh

   $ perl -pi -e '<some replacement regular expression>' <file or files>

grep
====

Frequently Used Options
-----------------------

.. code-block:: sh

   -P, --perl-regexp         PATTERN is a Perl regular expression
   -H, --with-filename       print the filename for each match
   -R, -r, --recursive       equivalent to --directories=recurse
   -B, --before-context=NUM  print NUM lines of leading context
   -A, --after-context=NUM   print NUM lines of trailing context
   -C, --context=NUM         print NUM lines of output context
   -i, --ignore-case         ignore case distinctions

.. code-block:: sh

   $ grep -r -i -P -B 3 -A 3 "orbitz" *

Find duplicate words
--------------------

The following will print out duplicate words cases such as ``the the``::

   $ grep -E "\b(\w+)\b \b\1\b" *.lyx


Searching in all files recursively using ``grep -r``
----------------------------------------------------

.. code-block:: sh

   $ grep -r "ramesh" *


From shell history, find items with rename in it
------------------------------------------------

.. code-block:: sh

   $ history | grep 'rename'

